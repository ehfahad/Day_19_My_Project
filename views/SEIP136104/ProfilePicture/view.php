<?php

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\ProfilePicture\ImageUpload;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$obj = new ImageUpload();
$obj->prepare($_GET);
$singleInfo = $obj->view();
//Utility::dd($singleInfo)
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Details</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>User Details</h2>
        <div class="form-group">
            <label>Username:</label><br>
            <input type="text" value="<?php echo $singleInfo->name ?>" disabled>
        </div>
        <div class="form-group">
            <label>Profile Picture:</label><br>
            <img src="../../../Resources/Images/<?php echo $singleInfo->images ?>" height="100px" width="100px">

        </div>

        <a href="index.php" role="button" class="btn btn-primary">Done</a>
</div>

</body>
</html>

