<?php

session_start();

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\ProfilePicture\ImageUpload;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$obj = new ImageUpload();
$allInfo = $obj->trashed();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>User Trashlist</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <center><h2>User Trashlist</h2></center>
    <?php if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])): ?>
        <div class="alert alert-info" id="message">
            <center> <?php echo Message::message() ?> </center>
        </div>
    <?php endif; ?>

    <a href="index.php" class="btn btn-primary" role="button">Go to Homepage</a>

    <form id="multi_recover" action="recoverSelected.php" method="post">
        <br>
        <button type="submit" class="btn btn-primary">Recover Selected</button>
        <button type="button" id="multi_delete" class="btn btn-danger">Delete Selected</button>
      <table class="table">
        <thead>
        <tr>
            <th>Check Item</th>
            <th>SL</th>
            <th>ID</th>
            <th>Username</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $sl = 0;
        foreach ($allInfo as $info) { $sl++; ?>
            <tr class="success">
                <td>
                    <input type="checkbox" name="mark[]" value="<?php echo $info->id ?>">
                </td>
                <td><?php echo $sl ?></td>
                <td><?php echo $info->id  ?></td>
                <td><?php echo $info->name  ?></td>
                <td>
                    <img src="../../../Resources/Images/<?php echo $info->images ?>" alt="image" height="100px" width="100px">
                </td>
                <td>
                    <a href="recover.php?id=<?php echo $info->id ?>" class="btn btn-primary" role="button">Recover</a>
                    <a href="delete.php?id=<?php echo $info->id ?>" class="btn btn-danger" role="button">Delete</a>
                </td>
            </tr>
        <?php } ?>
    
        </tbody>
    </table>
    </form>
</div>

<script>
    $('#message').show().delay(1100).fadeOut();
    
    $('#multi_delete').on('click',function () {
        document.forms[0].action= "deleteSelected.php";
        $('#multi_recover').submit();
    })
</script>

</body>
</html>

